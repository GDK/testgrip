unit uUnitParser.Test;

interface

uses
  DUnitX.TestFramework, System.Classes, System.SysUtils, uUnitParser, uPascalDefs, System.Generics.Collections;

type
  [TestFixture]
  [Category('TestGrip')]
  TTestUnitParser = class
  private
    FMockUnitContent: TStrings;
    FUnitParser: TUnitParser;

    procedure BuildMockUnitContent;
  public
    [Setup]
    procedure Setup;

    [TearDown]
    procedure TearDown;

    [Test]
    procedure TestCorrectBeginAndEndLineNumber;
  end;

implementation

type
  TExpectedClassDefinition = record
    Name: string;
    BeginLineNumber: Integer;
    EndLineNumber: Integer;
  end;

  TExpectedClassDefinitions = Array[0..3] of TExpectedClassDefinition;

const
  MockUnitExpectedClassDefinition: TExpectedClassDefinitions = (
    (Name: 'ITestClass';  BeginLineNumber: 10; EndLineNumber: 14),
    (Name: 'TTestClass';  BeginLineNumber: 16; EndLineNumber: 21),
    (Name: 'ITestClass1'; BeginLineNumber: 23; EndLineNumber: 27),
    (Name: 'TTestClass1'; BeginLineNumber: 29; EndLineNumber: 34)
  );

procedure TTestUnitParser.Setup;
begin
  FUnitParser := TUnitParser.Create;
end;

procedure TTestUnitParser.TearDown;
begin
  FUnitParser.Free;

  if Assigned(FMockUnitContent) then
    FreeAndNil(FMockUnitContent);
end;

procedure TTestUnitParser.TestCorrectBeginAndEndLineNumber;
var
  DataStream: TStringStream;
  ActualClassDefinitions: TList<TClassDefinition>;
  ObjClassDefinition: TObject;
  I: Integer;
  ExpectedClassDefinition: TExpectedClassDefinition;
  ActualClassDefinition: TClassDefinition;
begin
  BuildMockUnitContent;

  DataStream := TStringStream.Create;
  try
    DataStream.WriteString(FMockUnitContent.Text);
    FUnitParser.ParseUnit(DataStream, True, False, False, True, True);

    ActualClassDefinitions := TList<TClassDefinition>.Create;
    try
      for I := 0 to FUnitParser.InterfaceClassList.Count - 1 do
      begin
        ObjClassDefinition := FUnitParser.InterfaceClassList.Objects[I];
        if (ObjClassDefinition is TClassDefinition) then
          ActualClassDefinitions.Add(ObjClassDefinition as TClassDefinition);
      end;

      Assert.AreEqual(
        Length(MockUnitExpectedClassDefinition),
        ActualClassDefinitions.Count, 'Did found the same amount of class definitions as expected');

      for I := 0 to Length(MockUnitExpectedClassDefinition) - 1 do
      begin
        ExpectedClassDefinition := MockUnitExpectedClassDefinition[I];
        ActualClassDefinition := ActualClassDefinitions[I];

        Assert.AreEqual(
          ExpectedClassDefinition.Name, ActualClassDefinition.Name,
          Format('ClassDefinition.Name.%d', [I]));

        Assert.AreEqual(
          ExpectedClassDefinition.BeginLineNumber, ActualClassDefinition.BeginLineNumber,
          Format('ClassDefinition.BeginLineNumber.%d', [I]));

        Assert.AreEqual(
          ExpectedClassDefinition.EndLineNumber, ActualClassDefinition.EndLineNumber,
          Format('ClassDefinition.EndLineNumber.%d', [I]));
      end;
    finally
      ActualClassDefinitions.Free;
    end;
  finally
    DataStream.Free;
  end;
end;

procedure TTestUnitParser.BuildMockUnitContent;
begin
  FMockUnitContent := TStringList.Create;
  FMockUnitContent.AddStrings([
    'unit Unit1.example;',
    '',
    'interface',
    '',
    'type',
    '  TInternalState = record',
    '    Status: string;',
    '  end;',
    '',
    '  ITestClass = interface',
    '    [''{FCE152CF-D4A7-49C8-B1DF-7A676E08B353}'']',
    '',
    '    procedure Test;',
    '  end;',
    '',
    '  TTestClass = class(TInterfacedObject, ITestClass)',
    '  private',
    '    State: TInternalState;',
    '  public',
    '    procedure Test;',
    '  end;',
    '',
    '  ITestClass1 = interface',
    '    [''{3C8322AE-B313-4700-9A72-7F2FE9954FDF}'']',
    '',
    '    procedure Test;',
    '  end;',
    '',
    '  TTestClass1 = class(TInterfacedObject, ITestClass1)',
    '  private',
    '    FTestValue: string;',
    '  public',
    '    procedure Test;',
    '  end;',
    '',
    '  TTestRcd = record',
    '    Value: TDateTime;',
    '  end;',
    '',
    'implementation',
    '',
    '{ TTestClass }',
    '',
    'procedure TTestClass.Test;',
    'begin',
    '',
    'end;',
    '',
    '{ TTestClass1 }',
    '',
    'procedure TTestClass1.Test;',
    'begin',
    '  FTestValue := Self.ClassName;',
    'end;',
    '',
    'end.'
  ]);
end;

initialization
  TDUnitX.RegisterTestFixture(TTestUnitParser);

end.
