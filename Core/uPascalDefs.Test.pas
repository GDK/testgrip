unit uPascalDefs.Test;

interface

uses
  DUnitX.TestFramework, uPascalDefs, System.SysUtils;

type
  [TestFixture]
  [Category('TestGrip')]
  TTestClassDefinition = class
  private
    FEmptyClassDefinition: TClassDefinition;
  public
    [Setup]
    procedure Setup;

    [TearDown]
    procedure TearDown;

    [Test]
    procedure TesClassDefinitionValuesBeginAndEndLineNumberConstructorRawDefinitions;

    [Test]
    [TestCase('-1;-1','-1,-1')]
    [TestCase('17;15','17,15')]
    [TestCase('33;102','33,102')]
    procedure TestClassDefinitionValuesBeginAndEndLineNumberConstructorOtherDefinition(const BeginLineNumber, EndLineNumber: Integer);

    [Test]
    [TestCase('-1,-1,-1,true','-1,-1,-1,true')]
    [TestCase('0,12,9,true','0,12,9,true')]
    [TestCase('41,81,9,false','41,81,9,false')]
    [TestCase('23,64,37,true','23,64,37,true')]
    [TestCase('12,56,97,false','12,56,97,false')]
    procedure TestClassDefinitionLineInClassDefinition(
      const BeginLineNumber, EndLineNumber, LineNumber: Integer; const ExpectedResult: Boolean);

    [Test]
    procedure TestGenericMethod;

    [Test]
    procedure TestGenericSignature;
  end;

implementation

procedure TTestClassDefinition.Setup;
begin
  FEmptyClassDefinition := TClassDefinition.Create;
end;

procedure TTestClassDefinition.TearDown;
begin
  FEmptyClassDefinition.Free;
end;

procedure TTestClassDefinition.TesClassDefinitionValuesBeginAndEndLineNumberConstructorRawDefinitions;
var
  CreatedWithConstructorRawDefinition: TClassDefinition;
begin
  CreatedWithConstructorRawDefinition := TClassDefinition.Create('');
  try
    Assert.AreEqual(-1, CreatedWithConstructorRawDefinition.BeginLineNumber, 'BeginLineNumber');
    Assert.AreEqual(-1, CreatedWithConstructorRawDefinition.EndLineNumber, 'EndLineNumber');
  finally
    CreatedWithConstructorRawDefinition.Free;
  end;
end;

procedure TTestClassDefinition.TestClassDefinitionValuesBeginAndEndLineNumberConstructorOtherDefinition(
  const BeginLineNumber, EndLineNumber: Integer);
var
  OtherClassDefinition: TClassDefinition;
  CreatedWithConstructorOtherDefinition: TClassDefinition;
begin
  OtherClassDefinition := TClassDefinition.Create('');
  try
    OtherClassDefinition.BeginLineNumber := BeginLineNumber;
    OtherClassDefinition.EndLineNumber := EndLineNumber;

    CreatedWithConstructorOtherDefinition := TClassDefinition.Create(OtherClassDefinition);
    try
      Assert.AreEqual(OtherClassDefinition.BeginLineNumber, CreatedWithConstructorOtherDefinition.BeginLineNumber, 'BeginLineNumber');
      Assert.AreEqual(OtherClassDefinition.EndLineNumber, CreatedWithConstructorOtherDefinition.EndLineNumber, 'EndLineNumber');
    finally
      CreatedWithConstructorOtherDefinition.Free;
    end;
  finally
    OtherClassDefinition.Free;
  end;
end;

procedure TTestClassDefinition.TestClassDefinitionLineInClassDefinition(
      const BeginLineNumber, EndLineNumber, LineNumber: Integer; const ExpectedResult: Boolean);
begin
  FEmptyClassDefinition.BeginLineNumber := BeginLineNumber;
  FEmptyClassDefinition.EndLineNumber := EndLineNumber;

  Assert.AreEqual(ExpectedResult, FEmptyClassDefinition.LineInClassDefinition(LineNumber),
    Format('%d <= %d <= %d', [BeginLineNumber, LineNumber, EndLineNumber])
  );
end;

procedure TTestClassDefinition.TestGenericMethod;
var
  mdef: TMethodDefinition;
begin
  mdef := TMethodDefinition.Create('function Map<T, S>(const List: TList<T>; const MapFunc: _Func<T, S>): TList<S>', False, TClassScope.csPublic);
  Assert.AreEqual('Map<T, S>', mdef.DefMethodName, 'Method name');
  Assert.AreEqual('TList<S>', mdef.Functype, 'Return type');
  Assert.AreEqual('List'#13#10'MapFunc'#13#10, mdef.Parameters.Text, 'Param names');
  Assert.AreEqual('TList<T>'#13#10'_Func<T, S>'#13#10, mdef.ParamTypes.Text, 'Param types');
end;

procedure TTestClassDefinition.TestGenericSignature;
var
  mdef: TMethodDefinition;
begin
  mdef := TMethodDefinition.Create('tlist<s> _.map<t, s>(tlist<t>,_func<t, s>)', True, TClassScope.csPublic);
  Assert.AreEqual('map<t, s>', mdef.DefMethodName, 'Method name');
  Assert.AreEqual('tlist<s>', mdef.Functype, 'Return type');
  Assert.AreEqual(2, mdef.Parameters.Count, 'Param count');
  Assert.AreEqual('tlist<t>'#13#10'_func<t, s>'#13#10, mdef.ParamTypes.Text, 'Param types');
end;

initialization
  TDUnitX.RegisterTestFixture(TTestClassDefinition);

end.
